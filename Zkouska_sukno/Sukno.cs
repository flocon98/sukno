﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zkouska_sukno {
    class Osoba:IPlocha {
        public int vek;
        public int vyska;
        public string pohlavi;
        public Osoba(int vek, int  vyska, string pohlavi) {
            this.pohlavi = pohlavi;
            this.vek = vek;
            this.vyska = vyska;
        }

        public double sdelPlochu() {
            if (pohlavi =="zena") {
                return (vyska * 0.5) * 2;
            }
            else {
                return (vyska * 0.5);
            };
        }
    }
    abstract class Zvire:IPlocha {
        public int vaha;

        protected Zvire(int vaha) {
            this.vaha = vaha;
        }

        public virtual double sdelPlochu() {    //dedicnost - mela by byt virtual
            return 2*vaha;
        }
    }
    class Pes:Zvire, IPlocha {
        public int cena;
        public Pes(int vaha, int cena) :base(vaha) {
            this.vaha = vaha;
            this.cena = cena;
        }

        public override double sdelPlochu() {
            return base.sdelPlochu() + (cena * 0.1);
        }
    }
    class Dum:IPlocha {
        int pocetOken;
        int pudorys;
        public Dum(int pocetOken, int pudorys) {
            this.pocetOken = pocetOken;
            this.pudorys = pudorys;
        }

        public double sdelPlochu() {
           return pudorys * 2.5;
        }
    }
    class Auto:IPlocha {
        int pocetKol;
        public Auto(int pocetKol) {
            this.pocetKol = pocetKol;
        }

        public double sdelPlochu() {
            return pocetKol * 3.6;
        }
    }
    interface IPlocha {
         double sdelPlochu();
    }
    class Sukno {
        static void Mainx(string[] args) {
            Osoba o1 = new Osoba(50, 185, "muz");
            Osoba o2 = new Osoba(35, 168, "zena");
            Pes p12 = new Pes(20, 15000);
            Dum d1 = new Dum(10, 150);
            Auto a1 = new Auto(4);
            IPlocha[] polePloch = new IPlocha[5] { o1, o2, p12, d1, a1 };
            foreach (IPlocha item in polePloch) {
                Console.WriteLine(item.GetType().Name + " potrebuje " + item.sdelPlochu() + " m sukna");
            }
            Console.WriteLine(((Osoba)polePloch[0]).pohlavi);

            Console.WriteLine();
            List<IPlocha> listPloch = new List<IPlocha>() { o1, o2, p12, d1, a1 };
            foreach (IPlocha item in polePloch) {
                Console.WriteLine(item.GetType().Name + " potrebuje " + item.sdelPlochu() + " m sukna");
            }
        }
    }
}
