﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zkouska_oop {
    class Osoba : Iplocha {
        public int vek, vyska;
        public string pohlavi;
        public Osoba(int vek, int vyska, string pohlavi) {
            this.vek = vek;
            this.vyska = vyska;
            this.pohlavi = pohlavi;
        }
        public virtual double sdelPlochu() {
            if (pohlavi == "M") {
                return Math.Round((vyska / 2.0), 2);        //pro muze
            } else if (pohlavi == "F") {
                return Math.Round((vyska / 2.0), 2) * 2;     //pro zeny
            } else {
                return -1.0;
            }
        }
    }
    abstract class Zvire : Iplocha {
        public int vaha;
        public Zvire(int vaha) {
            this.vaha = vaha;
        }
        public Zvire() {
        }
        public virtual double sdelPlochu() {
            return vaha * 2.0;
        }
    }
    class Pes : Zvire, Iplocha {
        public int cena;
        public Pes(int vaha, int cena) : base(vaha) {
            this.cena = cena;
        }
        public override double sdelPlochu() {
            return base.sdelPlochu() * (cena * 0.1);
        }
    }
    class Dum : Iplocha {
        public int pocetOken;
        public int pudorys;
        public Dum(int pocetOken, int pudorys) {
            this.pocetOken = pocetOken;
            this.pudorys = pudorys;
        }
        public double sdelPlochu() {
            return (double)pudorys;
        }
    }
    interface Iplocha {
        double sdelPlochu();
    }
    class Auto : Iplocha {
        int rychlost;
        public Auto(int rychlost) {
            this.rychlost = rychlost;
        }
        public double sdelPlochu() {
            return rychlost * 2.5;
        }
    }
    public class Testovaci {
        public static void Mainx() {
            double celkovaPlocha = 0;
            Osoba o1 = new Osoba(25, 185, "M");     //muz
            Osoba o2 = new Osoba(25, 165, "F");     //zena
            Pes p1 = new Pes(15, 5000);
            Dum d1 = new Dum(16, 200);
            Auto a1 = new Auto(150);
            Iplocha[] databazePloch = new Iplocha[10];
            databazePloch[0] = o1;
            databazePloch[1] = p1;
            databazePloch[2] = d1;
            databazePloch[3] = a1;
            databazePloch[5] = o2;
            for (int i = 0; i < databazePloch.Length; i++) {
                if (databazePloch[i] != null) {
                    Console.WriteLine(databazePloch[i].GetType().Name + " ma plochu " + databazePloch[i].sdelPlochu());
                    celkovaPlocha += databazePloch[i].sdelPlochu();
                }
            }
            Console.WriteLine("celkova plocha je: " +celkovaPlocha);
            Console.WriteLine("Vypsana datova slozka vyska tridy Osoba " + ((Osoba)(databazePloch[0])).vyska);
            List <Iplocha> kolekcePloch = new List<Iplocha>();
            kolekcePloch.Add(o1);
            kolekcePloch.Add(o2);
            kolekcePloch.Add(p1);
            kolekcePloch.Add(d1);
            kolekcePloch.Add(a1);
            foreach(Iplocha x in kolekcePloch) {
                Console.WriteLine(x.GetType().Name + " ma plochu " + x.sdelPlochu());
            }
        }   
    }
}
