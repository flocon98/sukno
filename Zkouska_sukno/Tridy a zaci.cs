﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zkouska_sukno {
    class Zak {
        public string jmeno;
        public Trida mojeTrida;
        public Zak(string jmeno) {
            this.jmeno = jmeno;
        }
        public Zak(string jmeno, Trida trida) {
            this.jmeno = jmeno;
            this.mojeTrida = trida;
        }
    }
    class Trida {
        public string nazevTridy;
        public int pocetZaku;
        public Zak[] seznamZakuPole = new Zak[35];
        public List<Zak> seznamZakuList = new List<Zak>();
        public Trida(string nazev) {
            this.nazevTridy = nazev;
            pocetZaku = 0;
        }
        public void vypisTridy() {
            Console.WriteLine($"Zaky tridy {nazevTridy} jsou:");
            foreach (Zak z in seznamZakuPole) {
                if (z!=null) {
                    Console.WriteLine(z.jmeno);
                }
            }
        }
        public void zaradDoTridyPole(Zak zak) {
            seznamZakuPole[pocetZaku] = zak;
            zak.mojeTrida = this;
            pocetZaku++;
            if (pocetZaku>33) {
                throw new ArgumentOutOfRangeException("Mas moc zaku ve tride!");
            }
        }
        public void zaradDoTridyList(Zak zak) {
            seznamZakuList.Add(zak);
        }
    }
    class Tridy_a_zaci {
        static void Main(string[] args) {
            Zak z1 = new Zak("Michal");
            Zak z2 = new Zak("Martin");
            Zak z3 = new Zak("Pavel");
            Zak z4 = new Zak("Milan");
            Zak z5 = new Zak("Ivan");
            Trida t1 = new Trida("1A");
            Trida t2 = new Trida("1B");
            t1.zaradDoTridyPole(z1);
            t1.zaradDoTridyPole(z2);
            t1.zaradDoTridyPole(z3);
            t2.zaradDoTridyPole(z4);
            t2.zaradDoTridyPole(z5);
            t1.vypisTridy();
            t2.vypisTridy();
            Zak z6 = new Zak("Kamil");
            t1.zaradDoTridyPole(z6);
            Console.WriteLine($"Jmenuje se {z6.jmeno} a chodi do tridy {z6.mojeTrida.nazevTridy}");
            Console.WriteLine($"Jmenuje se {z1.jmeno} a chodi do tridy {z1.mojeTrida.nazevTridy}");
        }
    }
}
